package com.example.android.quizzapp;

/**
 *
 * this class defines the model for the adapter
 */

public class TriviaObject {
    public int triviaId;
    public int itemCount;


    public TriviaObject(int triviaId, int itemCount) {

        this.triviaId = triviaId;
        this.itemCount = itemCount;
    }

    public int getTrivia() {
        return triviaId;
    }

    public int getResource() {
        return itemCount;
    }
}
