package com.example.android.quizzapp;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;


public class AndroidTriviaFragment extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup convertView = (ViewGroup) inflater.inflate(R.layout.android_trivia, container, false);

        ArrayList<TriviaObject> androidTrivia = new ArrayList<TriviaObject>();
        androidTrivia.add(triviaObj(R.string.android1));
        androidTrivia.add(triviaObj(R.string.android2));
        androidTrivia.add(triviaObj(R.string.android3));
        androidTrivia.add(triviaObj(R.string.android4));
        androidTrivia.add(triviaObj(R.string.android5));
        androidTrivia.add(triviaObj(R.string.android6));
        androidTrivia.add(triviaObj(R.string.android7));
        androidTrivia.add(triviaObj(R.string.android8));
        androidTrivia.add(triviaObj(R.string.android9));
        androidTrivia.add(triviaObj(R.string.android10));


        // Create the adapter to convert the array to views
        TriviaListAdapter adapter = new TriviaListAdapter(getActivity(), androidTrivia);
        // Attach the adapter to a ListView
        ListView listView = (ListView) convertView.findViewById(R.id.list);
        listView.setAdapter(adapter);

        return convertView;
    }

    public TriviaObject triviaObj(int info) {
        return new TriviaObject(info, R.drawable.dancing_android);
    }
}