package com.example.android.quizzapp;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;


public class UsefulResourcesFragment extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.useful_resources, container, false);
        ArrayList<TriviaObject> resources = new ArrayList<TriviaObject>();

        resources.add(triviaObj(R.string.res1));
        resources.add(triviaObj(R.string.res2));
        resources.add(triviaObj(R.string.res3));
        resources.add(triviaObj(R.string.res4));
        resources.add(triviaObj(R.string.res5));
        resources.add(triviaObj(R.string.res6));
        resources.add(triviaObj(R.string.res7));
        resources.add(triviaObj(R.string.res8));
        resources.add(triviaObj(R.string.res9));
        resources.add(triviaObj(R.string.res10));
        resources.add(triviaObj(R.string.res11));
        resources.add(triviaObj(R.string.res12));
        resources.add(triviaObj(R.string.res13));

        // Create the adapter to convert the array to views
        UsefulResListAdapter adapter = new UsefulResListAdapter(getActivity(), resources);
        // Attach the adapter to a ListView
        ListView listView = (ListView) rootView.findViewById(R.id.list);
        listView.setAdapter(adapter);

        return rootView;
    }

    public TriviaObject triviaObj(int info) {
        return new TriviaObject(info, R.drawable.resources);
    }
}