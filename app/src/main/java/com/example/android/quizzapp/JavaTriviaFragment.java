package com.example.android.quizzapp;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;

public class JavaTriviaFragment extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup convertView = (ViewGroup) inflater.inflate(R.layout.java_trivia, container, false);

        ArrayList<TriviaObject> javaTrivia = new ArrayList<TriviaObject>();
        javaTrivia.add(triviaObj(R.string.jTrivia1));
        javaTrivia.add(triviaObj(R.string.jTrivia2));
        javaTrivia.add(triviaObj(R.string.jTrivia3));
        javaTrivia.add(triviaObj(R.string.jTrivia4));
        javaTrivia.add(triviaObj(R.string.jTrivia5));
        javaTrivia.add(triviaObj(R.string.jTrivia6));
        javaTrivia.add(triviaObj(R.string.jTrivia7));
        javaTrivia.add(triviaObj(R.string.jTrivia8));
        javaTrivia.add(triviaObj(R.string.jTrivia9));

        // Create the adapter to convert the array to views
        TriviaListAdapter adapter = new TriviaListAdapter(getActivity(), javaTrivia);
    // Attach the adapter to a ListView
        ListView listView = (ListView) convertView.findViewById(R.id.list);
        listView.setAdapter(adapter);

        return convertView;
    }

    public TriviaObject triviaObj (int info){
        return new TriviaObject(info, R.drawable.duke_icon);
    }
}
