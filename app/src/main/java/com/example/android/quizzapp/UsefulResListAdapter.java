package com.example.android.quizzapp;

import android.content.Context;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class UsefulResListAdapter extends ArrayAdapter<TriviaObject> {
    public UsefulResListAdapter(Context context, ArrayList<TriviaObject> item) {
        super(context, R.layout.res_list_item, item);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        final TriviaObject currentItem = getItem(position);

        // Check if an existing view is being reused, otherwise inflate the view
        ViewHolder viewHolder; // view lookup cache stored in tag
        if (convertView == null) {
            // If there's no view to re-use, inflate a brand new view for row
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.res_list_item, parent, false);

            viewHolder.itemCount = (TextView) convertView.findViewById(R.id.itemCount);
            viewHolder.info = (TextView) convertView.findViewById(R.id.element);


            // Cache the viewHolder object inside the fresh view
            convertView.setTag(viewHolder);
        } else {
            // View is being recycled, retrieve the viewHolder object from tag
            viewHolder = (ViewHolder) convertView.getTag();
        }

        // Populate the data from the data object via the viewHolder object
        // into the template view.
        viewHolder.itemCount.setText(String.valueOf(position + 1) + ".");
        viewHolder.info.setText(currentItem.getTrivia());
        viewHolder.info.setMovementMethod(LinkMovementMethod.getInstance());

        // Return the completed view to render on screen
        return convertView;
    }

    // View lookup cache
    //we use a ViewHolder to avoid calling findViewById everytime the list is accessed
    //this way the result of this method are kept in cache
    private static class ViewHolder {
        TextView itemCount;
        TextView info;
    }
}