package com.example.android.quizzapp;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.content.Context.INPUT_METHOD_SERVICE;

public class QuestionsAndroid extends Fragment {

    @BindView(R.id.android_result)
    Button android_result;
    @BindView(R.id.android_send_email)
    Button send_email;
    @BindView(R.id.rb12)
    RadioButton q1rb2;
    @BindView(R.id.rb34)
    RadioButton q3rb4;
    @BindView(R.id.cb21)
    CheckBox q2cb1;
    @BindView(R.id.cb22)
    CheckBox q2cb2;
    @BindView(R.id.cb23)
    CheckBox q2cb3;
    @BindView(R.id.cb24)
    CheckBox q2cb4;
    @BindView(R.id.cb51)
    CheckBox q5cb1;
    @BindView(R.id.cb52)
    CheckBox q5cb2;
    @BindView(R.id.cb53)
    CheckBox q5cb3;
    @BindView(R.id.cb54)
    CheckBox q5cb4;
    @BindView(R.id.cb55)
    CheckBox q5cb5;
    @BindView(R.id.editable_android_q4)
    EditText q4answer;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.questions_android, container, false);
        ButterKnife.bind(this, rootView);

        QuizzActivity main = (QuizzActivity) getActivity();
        final String name = main.getMyData();

        // Button java_result = (Button) rootView.findViewById(R.id.java_result);
        android_result.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                showResult();
            }
        });
        send_email.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                sendEmail(name);
            }
        });

        //hide keyboard on touch outside EditTExt
        rootView.findViewById(R.id.questions_android_main_layout).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(q4answer.getWindowToken(), 0);
                return true;
            }
        });

        return rootView;
    }

    public void sendEmail(String name) {
        String message = "Thank you " + name + " for trying out our app :) \n You've scored " + calculateResult() + " Android Points.\n Happy coding!";
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:")); // only email apps should handle this
        intent.putExtra(Intent.EXTRA_EMAIL, "");
        intent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.mail_subject_android) + " " + name);
        intent.putExtra(Intent.EXTRA_TEXT, message);
        if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
            startActivity(intent);
        }
    }

    public void showResult() {
        int androidPoints = calculateResult();
        String result = "You have " + androidPoints + " of 5 points.";
        Toast.makeText(getActivity(), result, Toast.LENGTH_LONG).show();
    }

    public int calculateResult() {
        int androidPoints = 0;

        //questions 1 and 3
        androidPoints += onRadioClick(q1rb2);
        androidPoints += onRadioClick(q3rb4);

        //questions 2 and 5
        if (checkIfClicked(q2cb1) && checkIfClicked(q2cb2) && checkIfClicked(q2cb3) && !checkIfClicked(q2cb4)) {
            androidPoints += 1;
        }
        if (checkIfClicked(q5cb1) && checkIfClicked(q5cb2) && checkIfClicked(q5cb4) && checkIfClicked(q5cb5) && !checkIfClicked(q5cb3)) {
            androidPoints += 1;
        }
        //questions 4
        String answer = q4answer.getText().toString();
        if (answer.trim().equals("new")) {
            androidPoints += 1;
        }
        return androidPoints;
    }

    public int onRadioClick(View view) {
        int radioPoints = 0;
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();
        // Check which radio button was clicked
        switch (view.getId()) {
            case R.id.rb12:
                if (checked)
                    radioPoints += 1;
                break;
            case R.id.rb34:
                if (checked)
                    radioPoints += 1;
                break;
            default:
                break;
        }
        return radioPoints;
    }

    public boolean checkIfClicked(View view) {
        return ((CheckBox) view).isChecked();
    }
}