package com.example.android.quizzapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity{
    String name = "";
    EditText clientName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Intent startQuizz = new Intent(this, QuizzActivity.class);
        final Intent readTrivia = new Intent(this, PagerTrivia.class);

        Button start = (Button) findViewById(R.id.start);
        clientName = (EditText) findViewById(R.id.name);
        start.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view){

                name = clientName.getText().toString();

                if(name.isEmpty() || name.equals("") || name.length() == 0){
                    Toast requestName = Toast.makeText(getApplicationContext(),"Please enter your name :)", Toast.LENGTH_SHORT);
                    requestName.show();
                }else{
                    Toast helloClient = Toast.makeText(getApplicationContext(),"Hello " + name, Toast.LENGTH_SHORT);
                    helloClient.show();
                    startQuizz.putExtra("name",name);
                    startActivity(startQuizz);
                }

            }


        });

        Button trivia = (Button) findViewById(R.id.trivia);
        trivia.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view){
                startActivity(readTrivia);
            }
        });

        //hide keyboard on touch outside EditTExt
        findViewById(R.id.main_relative_layout).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(clientName.getWindowToken(), 0);
                return true;
            }
        });


    }

}
