package com.example.android.quizzapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import butterknife.BindView;
import butterknife.ButterKnife;


public class QuizzJavaPrep extends Fragment {
    @BindView(R.id.goJavaTrivia)
    Button goJavaTrivia;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.prep_java, container, false);
        ButterKnife.bind(this, rootView);

        goJavaTrivia.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), PagerTrivia.class);
                startActivity(intent);
            }
        });

        return rootView;
    }
}