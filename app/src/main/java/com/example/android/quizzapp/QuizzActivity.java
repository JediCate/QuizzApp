package com.example.android.quizzapp;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

public class QuizzActivity extends AppCompatActivity {
    /**
     * The number of pages (wizard steps) to show in this demo.
     */
    private static final int NUM_PAGES = 4;
    private static final String TAG = "QuizzActivity.java";
    String name = "";
    /**
     * The pager widget handles animation and allows swiping horizontally to access previous
     * and next wizard steps.
     */
    private ViewPager mPager;
    /**
     * The pager adapter provides the pages to the view pager widget.
     */
    private PagerAdapter mPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quizz);

        name = getIntent().getExtras().getString("name");
        // Setting up the viewpager and the tablayout
        mPager = (ViewPager) findViewById(R.id.container);

        mPagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());
        mPager.setAdapter(mPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mPager);
        //change the swipe type of animation
        mPager.setPageTransformer(true, new DepthPageTransformer());
    }


    //sending the name to the fragment activities
    public String getMyData() {
        return name;
    }

    @Override
    public void onBackPressed() {
        if (mPager.getCurrentItem() == 0) {
            // If the user is currently looking at the first step, allow the system to handle the
            // Back button. This calls finish() on this activity and pops the back stack.
            super.onBackPressed();
        } else {
            // Otherwise, select the previous step.
            mPager.setCurrentItem(mPager.getCurrentItem() - 1);
        }
    }

    /**
     * A simple pager adapter that represents 3 ScreenSlidePageFragment objects, in
     * sequence.
     */
    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
        private ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    QuizzJavaPrep tab1 = new QuizzJavaPrep();
                    return tab1;
                case 1:
                    QuestionsJava tab2 = new QuestionsJava();
                    return tab2;
                case 2:
                    QuizzAndroidPrep tab3 = new QuizzAndroidPrep();
                    return tab3;
                case 3:
                    QuestionsAndroid tab4 = new QuestionsAndroid();
                    return tab4;
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return NUM_PAGES;
        }

        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Java Prep";
                case 1:
                    return "???";
                case 2:
                    return "Android Prep";
                case 3:
                    return "???";
            }
            return null;
        }
    }
}
